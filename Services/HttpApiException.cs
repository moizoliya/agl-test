﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class HttpApiException :Exception
    {
        public HttpApiException(HttpResponseMessage httpResponse) :
                base($"An error occured. The api returned HTTP Status of {httpResponse.StatusCode.ToString()}")
        {

        }
    }
}
