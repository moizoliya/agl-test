﻿using System.Collections.Generic;
using Data;
using System.Configuration;
using System.Net.Http;
using System;
using System.Linq;

using Newtonsoft.Json;

namespace Services
{
    public class PetDataService : IPetDataService
    {
        private readonly string _apiUrl;
        public PetDataService()
        {
            var key = typeof(PetDataService).Name + ".Api.Url";
            _apiUrl = ConfigurationManager.AppSettings[key];

        }

        public PetDataService(string api)
        {
            _apiUrl = api;
        }
        public IEnumerable<PetOwner> GetPetOwners()
        {
            using (var httpClient = new HttpClient())
            {
                var httpResponse = httpClient.GetAsync(_apiUrl).Result;
                if (httpResponse.StatusCode==System.Net.HttpStatusCode.OK)
                {
                    var contentString = httpResponse.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<IEnumerable<PetOwner>>(contentString);
                    return result;
                }
                else
                {
                    throw new HttpApiException(httpResponse);
                }
            }
            
            
        }
 
        public IDictionary<string,IEnumerable<Pet>> GetPetDataGroupedByOwnerGender(params string[] petType)
        {
            var listData = GetPetOwners();
            var result = new Dictionary<string, IEnumerable<Pet>>();
            foreach (var item in listData)
            {

                if (item.Pets==null)
                {
                    continue;
                }

                var petList = petType.Length==0 ? item.Pets : item.Pets.Where(m => petType.Contains(m.Type));

                if (!petList.Any())
                {
                    continue;
                }

                if (result.ContainsKey(item.Gender))
                {
                    result[item.Gender] = result[item.Gender].Concat(petList);
                }
                else
                {
                    result[item.Gender] = petList;
                }
            }
            return result;
        }
        
    }
}
