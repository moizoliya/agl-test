﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services;
using System.Linq;
namespace UnitTest
{
    [TestClass]
    public class AppTest
    {
      
        [TestMethod]
        public void Can_Get_And_Parse_Data()
        {
            var dataService = new PetDataService();
            var listData = dataService.GetPetOwners();
            Assert.IsTrue(listData.Any(), "Can retrieve and parse data" );
        }


        [TestMethod]
        [ExpectedException((typeof(HttpApiException)))]
        public void Can_Throw_Exception()
        {
            var dataService = new PetDataService("http://agl-developer-test.azurewebsites.net/people.jsonx");
            var listData = dataService.GetPetOwners();
            Assert.IsTrue(listData.Any(), "Can retrieve and parse data");
        }

        [TestMethod]
        public void Can_Get_Data_By_Owner_Gender_OnlyForCats()
        {
            var dataService = new PetDataService();
            var petType = "Cat";
            var petData = dataService.GetPetDataGroupedByOwnerGender(petType);
            var totalCount = petData.SelectMany(m => m.Value).Count();
            var countWithPetTypeCat = petData.SelectMany(m => m.Value).Where(m => m.Type == petType).Count();
            Assert.AreEqual(totalCount, countWithPetTypeCat, $"Total Petcount and Pet count with type : {petType}");
        }




    }
}
