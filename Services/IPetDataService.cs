﻿using System.Collections.Generic;
using Data;

namespace Services
{
    public interface IPetDataService
    {
        IEnumerable<PetOwner> GetPetOwners();
        IDictionary<string, IEnumerable<Pet>> GetPetDataGroupedByOwnerGender(params string[] petType);
    }
}
