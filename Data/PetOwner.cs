﻿using System.Collections.Generic;

namespace Data
{
    public class PetOwner 
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public IEnumerable<Pet> Pets { get; set; }
    }

     
}
