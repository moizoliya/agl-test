﻿using Services;
using System;
using System.Linq;
namespace App
{
    class Program
    {
        static void Main(string[] args)
        {


            var dataService = new PetDataService();
            try
            {
                var petData = dataService.GetPetDataGroupedByOwnerGender("Cat");
                foreach (var item in petData)
                {
                    Console.WriteLine($"{item.Key}");
                    var pets = item.Value.OrderBy(m => m.Name);
                    foreach (var petItem in pets)
                    {
                        Console.WriteLine($"\t{petItem.Name}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            }
            

            Console.ReadKey();    

        }
        
    }
}
